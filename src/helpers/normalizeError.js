const APIError = require('../lib/APIError');

module.exports = (error) => {
  if (!(error instanceof APIError)) {
    return new APIError(error);
  }
  return error;
};
