const Knex = require('knex');

exports.getKnex = (config) => Knex({
    client: config.dialect,
    connection: {
      host: config.host,
      user: config.user,
      password: config.password,
      database: config.database
    },
    pool: { min: config.poolMin, max: config.poolMax }
  });
