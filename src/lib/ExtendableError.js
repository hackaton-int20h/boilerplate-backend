/**
 * @extends Error
 */
class ExtendableError extends Error {
  constructor(code, message, statusCode, isPublic) {
    super(code);
    this.name = this.constructor.name;
    this.code = code;
    this.statusCode = statusCode;
    this.isPublic = isPublic;
    this.message = message;
    this.isOperational = true;
    Error.captureStackTrace(this, this.constructor.name);
  }
}

module.exports = ExtendableError;
