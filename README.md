# Test task Int20h backend
|master|develop|
|:----|-----:|
| [![build master][build-master]](https://gitlab.com/hackaton-int20h/test-task/commits/master) | [![build master][build-develop]](https://gitlab.com/hackaton-int20h/test-task/commits/develop)|
| [![coverage master][cov-develop]](https://gitlab.com/hackaton-int20h/test-task/commits/master) | [![coverage develop][cov-develop]](https://gitlab.com/hackaton-int20h/test-task/commits/develop) |


![backend-icon](http://blog.siliconstraits.vn/wp-content/uploads/2015/05/Internship-hiring-03.png)

Very important words about us and main idea of this shit


### Structure

- [API](#api)
- [Install](#install)
- [Contributing rules](#contributing)

<a name="usage"></a>

<a name="api"></a>

 
<a name="install"></a>
### Install
```bash
git clone git@gitlab.com:hackaton-int20h/test-task.git
cd test-task
npm i --production
npm start
```


<a name="contributing"></a>

### Contributing

To start contributing do

```bash
git clone git@gitlab.com:hackaton-int20h/test-task.git
git checkout develop
git checkout -b <your-branch-name>
```

The project is developed in accordance with the [GitFlow][gitflow-docs] methodology.

##### What it means

1. All work you should do in your own **local** branch (naming is important, look below), then make pull request to **develop** branch
2. Your local branch should not have conflicts with repository **develop** branch. To avoid it, before push to repository, do:
   ```bash
   git pull origin develop
   # resolve all conflicts, if they exists
   git add --all
   git commit -m "fix conflicts"
   git push origin <your-branch-name>
   ```
3. We use next naming of branches:

| branch template                      | description                                                 |
| ------------------------------------ | ----------------------------------------------------------- |
| `feat/<short-feature-name>`          | new feature, ex. `feat-add-logger`                          |
| `fix/<short-fix-name>`               | fix of existing feature, ex. `fix-logger`                   |
| `refactor/<short-scope-description>` | refactor, linting, style changes, ex. `style-update-eslint` |
| `test/<short-scope-descriptiopn>`    | tests, ex. `test-db-connections`                            |
| `docs/<short-scope-descriptiopn>`    | documentation, ex. `test-db-connections`                    |

##### Important, before push

1. We use **eslint** with this [rules][eslint-rules] to lint code, before making pull
   request, lint your code:
   `bash npm run lint`
2. Before making pull request, run tests

   ```bash
   npm run test
   ```

[gitflow-docs]: https://gitversion.readthedocs.io/en/latest/git-branching-strategies/gitflow-examples/
[eslint-rules]: .eslintrc

[build-master]:https://gitlab.com/hackaton-int20h/test-task/badges/master/pipeline.svg
[cov-master]:https://gitlab.com/hackaton-int20h/test-task/badges/master/coverage.svg
[build-develop]:https://gitlab.com/hackaton-int20h/test-task/badges/develop/pipeline.svg
[cov-develop]:https://gitlab.com/hackaton-int20h/test-task/badges/develop/coverage.svg
