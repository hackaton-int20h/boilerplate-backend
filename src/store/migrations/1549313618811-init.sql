CREATE OR REPLACE FUNCTION trigger_set_TIMESTAMPTZ()
  RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$
  LANGUAGE plpgsql;
