const { loadConfig } = require('@zulus/config');
const configSchema = require('./config.schema');

module.exports = loadConfig(__dirname, configSchema);
