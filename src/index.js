const config = require('./config');
const Koa = require('koa');
const cors = require('koa-cors');
const helmet = require('koa-helmet');
const Logger = require('@zulus/logger');
const compress = require('koa-compress');
const connections = require('./helpers/connections');
const bodyParser = require('koa-bodyparser');
const httpStatus = require('http-status');
const routes = require('./routes');
const normalizeError = require('./helpers/normalizeError');
const { Model } = require('objection');

new Logger(config.logger).patchLoggers();
const knex = connections.getKnex(config.database);
Model.knex(knex);

const app = new Koa();
app.proxy = true;
app.context.ROOT = process.cwd();
app.context.config = config;
app.context.knex = knex;

app.use(helmet());
app.use(cors());
app.use(bodyParser());
app.use(compress());
app.use(async (ctx, next) => {
  try {
    ctx.compress = true;
    await next();
  } catch (err) {
    const normalizedError = normalizeError(err);
    if (normalizedError.statusCode === httpStatus.INTERNAL_SERVER_ERROR) {
      console.error(err);
    }
    normalizedError.patch(ctx);
  }
});

app.listen(app.context.config.app.port);

app.use(routes);
console.info(`Server work on ${app.context.config.app.port}`);
module.exports = app;
