module.exports = {
  isDev: true,
  logger: {
    name: 'app',
    timestamps: true,
    transports: [
      {
        type: 'console',
        level: 'debug'
      }
    ]
  },
  database: {
    host: 'localhost',
    port: 5432,
    user: 'user',
    password: 'password',
    dialect: 'pg',
    database: 'database'
  },
  app: {
    port: 3000
  }
};
