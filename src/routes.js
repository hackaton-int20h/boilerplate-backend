const Router = require('koa-router');
const ping = require('./handlers/ping');

const router = Router();
const apiRouter = Router();

apiRouter.get('/ping', ping);

router.use('/api/v1', apiRouter.routes());
module.exports = router.routes();
