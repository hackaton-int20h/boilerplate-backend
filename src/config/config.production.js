module.exports = {
  isDev: false,
  logger: {
    name: 'app',
    timestamps: true,
    transports: [
      {
        type: 'console',
        level: 'debug'
      },
      {
        type: 'file',
        level: 'info',
        filename: process.env.LOG_FILE,
        maxsize: 1024 ** 3,
        maxfiles: 5
      }
    ]
  },
  database: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    dialect: 'pg',
    database: process.env.DB_NAME
  },
  app: {
    port: 3000
  }
};
