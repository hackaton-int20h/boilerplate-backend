const Joi = require('joi');
const { defaultConfigs } = require('@zulus/config');

module.exports = Joi.object({
  isDev: Joi.boolean().default(false).optional(),
  env: Joi.string()
    .valid('development', 'production', 'test')
    .default(process.env.NODE_ENV)
    .optional(),
  logger: defaultConfigs.logger.generate({
    maxFiles: 5,
    maxFilesSize: 1024 ** 3,
    name: 'app'
  }),
  database: Joi.object({
    port: Joi.number(),
    host: Joi.string(),
    dialect: Joi.string(),
    password: Joi.string(),
    user: Joi.string(),
    database: Joi.string(),
    poolMin: Joi.number().min(0).default(0).optional(),
    poolMax: Joi.number().min(0).default(10).optional()
  }),
  app: Joi.object({
    port: Joi.number()
  })
});
