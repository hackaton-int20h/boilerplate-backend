module.exports = async (ctx, next) => {
  ctx.body = { status: 'up' };
  await next();
};
