const ExtendableError = require('./ExtendableError');
const httpStatus = require('http-status');

/**
 * Class representing an API error.
 * @extends ExtendableError
 */
class APIError extends ExtendableError {
  /**
   * Creates an API error.
   * @param {string} code - Error code.
   * @param {string} message - Error message
   * @param {number} statusCode  - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor({
                code,
                message,
                statusCode = httpStatus.INTERNAL_SERVER_ERROR,
                isPublic = true
              }) {
    if (!code) {
      code = httpStatus[statusCode];
    }
    super(code, message, statusCode, isPublic);
  }

  patch(ctx) {
    ctx.body = {
      error: {
        code: this.code,
        message: this.isPublic ? this.message : httpStatus[this.statusCode || 500],
        statusCode: this.statusCode,
        stack: ctx.config.app.isDev ? undefined : this.stack
      }
    };
    ctx.status = this.statusCode;
  }
}

module.exports = APIError;
